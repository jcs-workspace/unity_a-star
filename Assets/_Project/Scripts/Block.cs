/**
 * $File: Block.cs $
 * $Date: 2023-04-27 15:12:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class Block : MonoBehaviour
{
    /* Variables */

    public enum State
    { 
        None,
        Invalid,
        Current,
        Reached,
        Path,
    };

    [Separator("Check Variables")]

    [Tooltip("Block at the forward.")]
    [SerializeField]
    [ReadOnly]
    private Block mBlockForward = null;

    [Tooltip("Block at the back.")]
    [SerializeField]
    [ReadOnly]
    private Block mBlockBack = null;

    [Tooltip("Block at the left.")]
    [SerializeField]
    [ReadOnly]
    private Block mBlockLeft = null;

    [Tooltip("Block at the right.")]
    [SerializeField]
    [ReadOnly]
    private Block mBlockRight = null;

    [Separator("Runtime Variables")]

    [Tooltip("State of the block.")]
    [SerializeField]
    private State mState = State.None;

    [Tooltip("Extra cost to spend on this block.")]
    [SerializeField]
    [Range(1.0f, 30.0f)]
    private double mCost = 1.0;

    /* Setter & Getter */

    public State state 
    { 
        get { return mState; } 
        set { mState = value; Render(); }
    }
    public double Cost { get { return mCost; } }

    /* Functions */

    private void Awake()
    {
        BlockManager.instance.AddBlock(this);

        Render();
    }

    public void Render()
    {
        var bm = BlockManager.instance;
        var renderer = GetComponent<Renderer>();

        switch (mState)
        {
            case State.None:
                renderer.material = bm.MatDefault;
                break;
            case State.Invalid:
                renderer.material = bm.MatInvalid;
                break;
            case State.Current:
                renderer.material = bm.MatCurrent;
                break;
            case State.Reached:
                renderer.material = bm.MatReached;
                break;
            case State.Path:
                renderer.material = bm.MatPath;
                break;
        }
    }

    public void SetState(State e)
    {
        this.mState = e;
        Render();
    }

    public List<Block> Neighbors()
    {
        List<Block> neighbors = new ();

        Vector3 pos = this.transform.position;

        Vector3 forward = pos + Vector3.forward;
        Vector3 back = pos + Vector3.back;
        Vector3 right = pos + Vector3.right;
        Vector3 left = pos + Vector3.left;

        var bm = BlockManager.instance;

        mBlockForward = bm.GetBlock(forward.x, forward.z);
        mBlockBack = bm.GetBlock(back.x, back.z);
        mBlockRight = bm.GetBlock(right.x, right.z);
        mBlockLeft = bm.GetBlock(left.x, left.z);

        if (mBlockForward) neighbors.Add(mBlockForward);
        if (mBlockBack) neighbors.Add(mBlockBack);
        if (mBlockRight) neighbors.Add(mBlockRight);
        if (mBlockLeft) neighbors.Add(mBlockLeft);

        return neighbors;
    }
}
