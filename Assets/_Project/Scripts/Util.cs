using System.Collections.Generic;
using System.Linq;

namespace USqr
{
    public static class Util
    {
        /// <summary>
        /// Remove the empty slot in the array.
        /// </summary>
        /// <typeparam name="T"> Type of the List. </typeparam>
        /// <param name="inArray"> Array list. </param>
        /// <returns> Cleaned up Array object. </returns>
        public static T[] RemoveEmptySlot<T>(T[] inArray)
        {
            return RemoveEmptySlot(inArray.ToList()).ToArray();
        }
        public static List<T> RemoveEmptySlot<T>(List<T> inList)
        {
            List<T> newArray = new List<T>(inList.Count);

            for (int index = 0; index < inList.Count; ++index)
            {
                // Add itself if exists.
                if (inList[index] != null)
                    newArray.Add(inList[index]);
            }

            return newArray;
        }
    }
}
