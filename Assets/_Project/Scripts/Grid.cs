/**
 * $File: Grid.cs $
 * $Date: 2023-05-03 15:12:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright � 2023 by Shen, Jen-Chieh $
 */
using System.Collections.Generic;

public class Grid<T>
{
    private Dictionary<float, Dictionary<float, T>> mMap = new();
    public List<T> mItems = new();

    public int Count { get { return mItems.Count; } }
    public List<T> Items 
    { 
        get 
        {
            mItems = USqr.Util.RemoveEmptySlot(mItems);
            return mItems; 
        }
    }

    public void Set(float x, float z, T obj)
    {
        Dictionary<float, T> column;

        if (mMap.ContainsKey(x))
        {
            column = mMap[x];
        }
        else
        {
            column = new();
            mMap.Add(x, column);
        }

        if (column.ContainsKey(z))
        {
            column[z] = obj;
        }
        else
        {
            column.Add(z, obj);
        }

        mItems.Add(obj);
    }

    public T Get(float x, float z)
    {
        if (!mMap.ContainsKey(x))
            return default(T);

        Dictionary<float, T> column = mMap[x];

        if (!column.ContainsKey(z))
            return default(T);

        return column[z];
    }

    public bool ContainKey(float x, float z)
    {
        if (!mMap.ContainsKey(x))
            return false;

        Dictionary<float, T> column = mMap[x];

        return column.ContainsKey(z);
    }

    public void Clear()
    {
        mMap.Clear();
    }
}
