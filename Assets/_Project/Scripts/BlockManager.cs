/**
 * $File: BlockManager.cs $
 * $Date: 2023-04-27 16:22:45 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */
using System.Collections.Generic;
using System.Data.Common;
using MyBox;
using UnityEngine;
using UnityEngine.UIElements;

public class BlockManager : MonoBehaviour
{
    /* Variables */

    public static BlockManager instance = null;

    private Grid<Block> mMap = new();

    [Separator("Initialize Variables")]

    [Tooltip("Material assign when block is normal.")]
    [SerializeField]
    private Material mMatDefault = null;

    [Tooltip("Material assign when block is invalid.")]
    [SerializeField]
    private Material mMatInvalid = null;

    [Tooltip("Material assign when block is a neighbors.")]
    [SerializeField]
    private Material mMatCurrent = null;

    [Tooltip("Material assign when block is reached.")]
    [SerializeField]
    private Material mMatReached = null;

    [Tooltip("Material assign when block is a found path.")]
    [SerializeField]
    private Material mMatPath = null;

    /* Setter & Getter */
    public List<Block> Blocks { get { return mMap.Items; } }
    public Material MatDefault { get { return mMatDefault; } }
    public Material MatInvalid { get { return mMatInvalid; } }
    public Material MatCurrent { get { return mMatCurrent; } }
    public Material MatReached { get { return mMatReached; } }
    public Material MatPath { get { return mMatPath; } }

    /* Functions */

    private void Awake()
    {
        instance = this;
    }

#if UNITY_EDITOR
    private void Update()
    {
        TestKey();
    }

    private void TestKey()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Block b = instance.GetBlock(1, 1);

            print(b);
        }
    }
#endif

    /// <summary>
    /// Add a block to get manage.
    /// </summary>
    /// <param name="block"> The target block to add. </param>
    public void AddBlock(Block block)
    {
        Vector3 pos = block.transform.position;

        mMap.Set(pos.x, pos.z, block);
    }

    /// <summary>
    /// Get the block in position with row and column.
    /// </summary>
    /// <param name="x"> row </param>
    /// <param name="y"> column </param>
    /// <returns> Return a block at position; return null if not found. </returns>
    public Block GetBlock(float x, float z)
    {
        return mMap.Get(x, z);
    }
}
