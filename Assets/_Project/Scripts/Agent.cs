/**
 * $File: Agent.cs $
 * $Date: 2023-05-03 15:12:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright � 2023 by Shen, Jen-Chieh $
 */
using UnityEngine;
using MyBox;
using USqr;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using System.Linq;
using Compat;

public class Agent : MonoBehaviour
{
    /* Variables */

    public enum Algorithm
    {
        BreathFirstSearch,
        BreathFirstSearch_ExitEarly,
        UniformCostSearch,            // Dijkstra�s Algorithm
        GreedyBestFirstSearch,
        AStar,
    };

    private Grid<Block> mCameFrom = new();
    private Grid<double> mCostSoFar = new();

    [Separator("Check Variables")]

    [Tooltip("Target.")]
    [SerializeField]
    private Transform mTarget = null;

    [Tooltip("Frontier blocks.")]
    [SerializeField]
    [ReadOnly]
    private HashSet<Block> mFrontier = new();

    [Tooltip("Frontier blocks. (PriorityQueue)")]
    [SerializeField]
    [ReadOnly]
    private PriorityQueue<Block, double> mFrontierPQ = new();

    [Tooltip("Current block.")]
    [SerializeField]
    [ReadOnly]
    private Block mCurrent = null;

    [SerializeField]
    private int mCurrentIndex = 0;

    private Timer mTimer = null;

    [Separator("Initialize Variables")]

    [Tooltip("Algorithm to display.")]
    [SerializeField]
    private Algorithm mAgorithm = Algorithm.BreathFirstSearch;

    [Tooltip("Time to progress search.")]
    [SerializeField]
    [Range(0.0f, 10.0f)]
    private float mInterval = 0.5f;

    /* Setter & Getter */

    /* Functions */

    private void Start()
    {
        StartEvent();

        var pos = this.transform.position;
        mCurrent = BlockManager.instance.GetBlock(pos.x, pos.z);
        mFrontier.Add(mCurrent);
        mFrontierPQ.Enqueue(mCurrent, 0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            var bm = BlockManager.instance;
            foreach (var block in bm.Blocks)
            {
                if (block.state == Block.State.Invalid)
                    continue;

                block.state = Block.State.None;
            }
            ResetVariables();
            StartEvent();
        }
    }

    private void StartEvent()
    {
        switch (mAgorithm)
        {
            case Algorithm.BreathFirstSearch:
                mTimer = Timer.Register(mInterval, BreathFirstSearch, isLooped: true);
                break;
            case Algorithm.BreathFirstSearch_ExitEarly:
                mTimer = Timer.Register(mInterval, BreathFirstSearch_ExitEarly, isLooped: true);
                break;
            case Algorithm.UniformCostSearch:
                mTimer = Timer.Register(mInterval, UniformCostSearch, isLooped: true);
                break;
            case Algorithm.GreedyBestFirstSearch:
                mTimer = Timer.Register(mInterval, GreedyBestFirstSearch, isLooped: true);
                break;
            case Algorithm.AStar:
                mTimer = Timer.Register(mInterval, AStar, isLooped: true);
                break;
        }
    }

    private void ResetVariables()
    {
        mCurrentIndex = 0;
        // Prepare frontier.
        {
            mFrontier.Clear();
            var pos = this.transform.position;
            mCurrent = BlockManager.instance.GetBlock(pos.x, pos.z);
            mFrontier.Add(mCurrent);

            mFrontierPQ.Clear();
            mFrontierPQ.Enqueue(mCurrent, 0);
        }
        mCameFrom.Clear();
        mCostSoFar.Clear();
    }

    private void BreathFirstSearch()
    {
        if (mFrontier.Count <= mCurrentIndex)
        {
            Timer.Cancel(mTimer);
            ShowPath();
            return;
        }

        mCurrent.SetState(Block.State.Reached);  // leaving, set to reached

        mCurrent = mFrontier.ElementAt(mCurrentIndex);
        mCurrent.SetState(Block.State.Current);

        List<Block> neighbors = mCurrent.Neighbors();

        foreach (Block neighbor in neighbors)
        {
            if (neighbor.state == Block.State.Invalid)
                continue;

            // All come from current block!
            var pos = neighbor.transform.position;

            if (mFrontier.Contains(neighbor))
                continue;

            mFrontier.Add(neighbor);

            mCameFrom.Set(pos.x, pos.z, mCurrent);
        }

        ++mCurrentIndex;
    }

    private void BreathFirstSearch_ExitEarly()
    {
        if (mFrontier.Count <= mCurrentIndex)
        {
            Timer.Cancel(mTimer);
            ShowPath();
            return;
        }

        mCurrent.SetState(Block.State.Reached);  // leaving, set to reached

        mCurrent = mFrontier.ElementAt(mCurrentIndex);
        mCurrent.SetState(Block.State.Current);

        List<Block> neighbors = mCurrent.Neighbors();

        foreach (Block neighbor in neighbors)
        {
            if (neighbor.state == Block.State.Invalid)
                continue;

            // All come from current block!
            var pos = neighbor.transform.position;

            if (mFrontier.Contains(neighbor))
                continue;

            mFrontier.Add(neighbor);

            mCameFrom.Set(pos.x, pos.z, mCurrent);

            // Exit early!
            if (pos == mTarget.position)
            {
                neighbor.state = Block.State.Reached;
                Timer.Cancel(mTimer);
                ShowPath();
                break;
            }
        }

        ++mCurrentIndex;
    }

    private void UniformCostSearch()
    {
        if (mFrontierPQ.Count == 0)
        {
            Timer.Cancel(mTimer);
            ShowPath();
            return;
        }

        mCurrent.SetState(Block.State.Reached);  // leaving, set to reached

        mCurrent = mFrontierPQ.Dequeue();
        mCurrent.SetState(Block.State.Current);

        List<Block> neighbors = mCurrent.Neighbors();

        foreach (Block neighbor in neighbors)
        {
            if (neighbor.state == Block.State.Invalid)
                continue;

            // All come from current block!
            var pos = neighbor.transform.position;

            // First get the neighbor's block cost
            double costSoFar = mCostSoFar.Get(pos.x, pos.z);

            var curPos = mCurrent.transform.position;

            double currentCost = mCostSoFar.Get(curPos.x, curPos.z);
            double blockCost = neighbor.Cost;
            double newCost = currentCost + blockCost;

            if (costSoFar == default(double)  // If neighbor block never get set;
                || newCost < costSoFar)       // Or the newCost is more efficient.
            {
                // Use the more efficient cost!
                mFrontierPQ.Enqueue(neighbor, newCost);

                // Update the possible cost so far!
                mCostSoFar.Set(pos.x, pos.z, newCost);

                // Set neighbor (pos) to current block, it means the neighbor came
                // from this block!
                mCameFrom.Set(pos.x, pos.z, mCurrent);
            }

            // Exit early!
            if (pos == mTarget.position)
            {
                neighbor.state = Block.State.Reached;
                Timer.Cancel(mTimer);
                ShowPath();
                break;
            }
        }
    }

    private void GreedyBestFirstSearch()
    {
        if (mFrontierPQ.Count == 0)
        {
            Timer.Cancel(mTimer);
            ShowPath();
            return;
        }

        mCurrent.SetState(Block.State.Reached);  // leaving, set to reached

        mCurrent = mFrontierPQ.Dequeue();
        mCurrent.SetState(Block.State.Current);

        List<Block> neighbors = mCurrent.Neighbors();

        foreach (Block neighbor in neighbors)
        {
            if (neighbor.state == Block.State.Invalid)
                continue;

            // All come from current block!
            var pos = neighbor.transform.position;

            // First get the neighbor's block cost
            double costSoFar = mCostSoFar.Get(pos.x, pos.z);

            var tarPos = mTarget.position;

            if (costSoFar == default(double))  // If neighbor block never get set;
            {
                double distance = Vector3.Distance(tarPos, pos);

                // Use the more efficient cost!
                mFrontierPQ.Enqueue(neighbor, distance);

                // Update the possible cost so far!
                mCostSoFar.Set(pos.x, pos.z, distance);

                // Set neighbor (pos) to current block, it means the neighbor came
                // from this block!
                mCameFrom.Set(pos.x, pos.z, mCurrent);
            }

            // Exit early!
            if (pos == mTarget.position)
            {
                neighbor.state = Block.State.Reached;
                Timer.Cancel(mTimer);
                ShowPath();
                break;
            }
        }
    }

    private void AStar()
    {
        if (mFrontierPQ.Count == 0)
        {
            Timer.Cancel(mTimer);
            ShowPath();
            return;
        }

        mCurrent.SetState(Block.State.Reached);  // leaving, set to reached

        mCurrent = mFrontierPQ.Dequeue();
        mCurrent.SetState(Block.State.Current);

        List<Block> neighbors = mCurrent.Neighbors();

        foreach (Block neighbor in neighbors)
        {
            if (neighbor.state == Block.State.Invalid)
                continue;

            // All come from current block!
            var pos = neighbor.transform.position;

            // First get the neighbor's block cost
            double costSoFar = mCostSoFar.Get(pos.x, pos.z);

            var curPos = mCurrent.transform.position;

            double currentCost = mCostSoFar.Get(curPos.x, curPos.z);
            double blockCost = neighbor.Cost;
            double newCost = currentCost + blockCost;

            var tarPos = mTarget.position;

            if (costSoFar == default(double)  // If neighbor block never get set;
                || newCost < costSoFar)       // Or the newCost is more efficient.
            {
                double distance = Vector3.Distance(tarPos, pos);
                double priority = newCost + distance;

                // Use the more efficient cost!
                mFrontierPQ.Enqueue(neighbor, priority);

                // Update the possible cost so far!
                mCostSoFar.Set(pos.x, pos.z, newCost);

                // Set neighbor (pos) to current block, it means the neighbor came
                // from this block!
                mCameFrom.Set(pos.x, pos.z, mCurrent);
            }

            // Exit early!
            if (pos == mTarget.position)
            {
                neighbor.state = Block.State.Reached;
                Timer.Cancel(mTimer);
                ShowPath();
                break;
            }
        }
    }

    private void ShowPath()
    {
        var pos = mTarget.position;
        var bm = BlockManager.instance;

        // Get current
        Block current = bm.GetBlock(pos.x, pos.z);
        current.state = Block.State.Path;

        // Get came from
        Block block = mCameFrom.Get(pos.x, pos.z);

        while (block)
        {
            block.state = Block.State.Path;

            pos = block.transform.position;  // next block

            block = mCameFrom.Get(pos.x, pos.z);

            if (pos == this.transform.position)
                break;
        }
    }
}
