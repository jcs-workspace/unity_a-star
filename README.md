[![Unity Engine](https://img.shields.io/badge/unity-2022.2.8f1-black.svg?style=flat&logo=unity)](https://unity3d.com/get-unity/download/archive)

# Unity_A-star

Following the tutorial [Introduction to the A* Algorithm](https://www.redblobgames.com/pathfinding/a-star/introduction.html)
from Red Blob Games.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [🎮 Demo](#🎮-demo)
  - [BreathFirstSearch](#breathfirstsearch)
  - [BreathFirstSearch_ExitEarly](#breathfirstsearch_exitearly)
  - [UniformCostSearch](#uniformcostsearch)
  - [GreedyBestFirstSearch](#greedybestfirstsearch)
  - [AStar](#astar)
- [📌 Dependencies](#📌-dependencies)
- [Things to improve](#things-to-improve)

<!-- markdown-toc end -->


## 🎮 Demo

### BreathFirstSearch

<img src="etc/1_BreathFirstSearch.gif" width="40%" />

### BreathFirstSearch_ExitEarly

<img src="etc/2_BreathFirstSearch_ExitEarly.gif" width="40%" />

### UniformCostSearch

<img src="etc/3_UniformCostSearch.gif" width="40%" />

### GreedyBestFirstSearch

<img src="etc/4_GreedyBestFirstSearch.gif" width="40%" />

### AStar

<img src="etc/5_AStar.gif" width="40%" />

## 📌 Dependencies

These dependencies are not necessary, but I use it to get rid of distractions
so people who read the code can understand things better.

The order of this list is based on its importance.

- [PriorityQueue][]
- [UnityTimer](https://github.com/akbiggs/UnityTimer)
- [JCSUnity](https://github.com/jcs090218/JCSUnity)
- [MyBox](https://github.com/Deadcows/MyBox) - I use this just to practice and prepare to use it in my next project.

## Things to improve

~~- Change `PriorityQueue` to something more efficient to gain more performance.~~

Now uses .NET version, [PriorityQueue][].


[PriorityQueue]: https://github.com/FyiurAmron/PriorityQueue/blob/main/PriorityQueue.cs
